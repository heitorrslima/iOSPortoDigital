//
//  EditTripFormViewController.swift
//  MyTrip
//
//  Created by Heitor Lima on 16/06/18.
//  Copyright © 2018 Treinamento. All rights reserved.
//

import UIKit

class EditTripFormViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var title_bar_trip: UINavigationItem!
    @IBOutlet weak var title_trip: UITextField!
    @IBOutlet weak var image_trip: UIImageView!
    @IBOutlet weak var date_trip: UIDatePicker!
    @IBOutlet weak var description_trip: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //restore element
        let trip: Trip = repositorio.getTripByIndex(repositorio.getIndexTrip())
        title_trip.text = trip.getName()
        title_bar_trip.title = trip.getName()
        image_trip.image = trip.getImage()
        date_trip.date = trip.getDate()
        description_trip.text = trip.getDescription()
//         Do any additional setup after loading the view.
    }
    
    
    //Action for change picture input
    @IBAction func change_b(_ sender: Any) {
        //If galery is OK
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image =  info[UIImagePickerControllerOriginalImage] as? UIImage{
            image_trip.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    //button for save inputs in trip element
    @IBAction func save_changes(_ sender: Any) {
        
        //Create object trip
        let trip = Trip(title_trip.text!,description_trip.text,image_trip.image!,date_trip.date)
        //replace object in array
        repositorio.editTripByID(repositorio.getIndexTrip(), trip);
        
        //Perfom Segue to back show detailed element
        performSegue(withIdentifier: "back_edited_trip_segue_app", sender: self)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
