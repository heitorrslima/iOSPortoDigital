//
//  ViewControllerTableViewCell.swift
//  MyTrip
//
//  Created by Treinamento on 16/06/2018.
//  Copyright © 2018 Treinamento. All rights reserved.
//

import UIKit

class ViewControllerTableViewCell: UITableViewCell {

    @IBOutlet weak var imageTrip: UIImageView!
    @IBOutlet weak var titleTrip: UILabel!
    @IBOutlet weak var descriptionTrip: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
