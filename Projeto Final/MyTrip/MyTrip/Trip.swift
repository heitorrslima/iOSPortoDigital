//
//  Trip.swift
//  MyTrip
//
//  Created by Treinamento on 16/06/2018.
//  Copyright © 2018 Treinamento. All rights reserved.
//

import UIKit

class Trip{

    private var name:String;
    private var description:String;
    private var image:UIImage?;
    private var date:Date;
    
    init(_ name: String, _ description:String, _ image:UIImage?, _ date:Date) {
        self.name = name;
        self.description = description;
        self.image = image;
        self.date = date
    }
    
    init(_ name: String, _ description:String, _ image:UIImage?) {
        self.name = name;
        self.description = description;
        self.image = image;
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
        print("data não informada")
        self.date = dateFormatter.date(from: "17:00")!
    }
    
    func getName() -> String {
        return self.name;
    }
    
    func getDescription() -> String {
        return self.description;
    }
    
    func getDate() -> Date{
        return self.date
    }
    
    func setDescription(_ description: String) {
        self.description = description;
    }

    func getImage() -> UIImage? {
        return self.image;
    }
}
