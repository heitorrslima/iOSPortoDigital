//
//  ViewController.swift
//  MyTrip
//
//  Created by Treinamento on 16/06/2018.
//  Copyright © 2018 Treinamento. All rights reserved.
//

import UIKit

var repositorio = TripRepository();

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //Define o numero se seccoes da minha table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    //Define o tamanho da minha table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositorio.getArray().count
    }
    
    //retorna uma das celulas para polular a minha table view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripCell", for: indexPath) as! ViewControllerTableViewCell;
        let tripElement:Trip = repositorio.getArray()[indexPath.row];
        cell.titleTrip?.text = tripElement.getName();
        cell.descriptionTrip?.text = tripElement.getDescription();
        cell.imageTrip?.image = tripElement.getImage();
        return cell;
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        repositorio.setIndexTrip(indexPath.row) ;
        performSegue(withIdentifier: "tripSelectedSegue", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

