//
//  AddTripFormViewController.swift
//  MyTrip
//
//  Created by Heitor Lima on 16/06/18.
//  Copyright © 2018 Treinamento. All rights reserved.
//

import UIKit

class AddTripFormViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //Variables
    
    //title trip
    @IBOutlet weak var title_trip: UITextField!
    
    //image trip
    @IBOutlet weak var image_trip: UIImageView!
    
    //Date trip
    @IBOutlet weak var date_trip: UIDatePicker!
    
    //description trip
    @IBOutlet weak var description_trip: UITextView!
    
    
    //Function buttons
   
    //Function to search picture and store
    @IBAction func store_picture_b(_ sender: Any) {
    }

    //Add the new Trip in repository
    @IBAction func add_new_trip(_ sender: Any) {
        //init a new trip
        let tripElement = Trip(title_trip.text!,description_trip.text!, image_trip.image!, date_trip.date)
        
        //add the element to the repository
        repositorio.addTrip(trip: tripElement)
        
        //Perform Segue from addForm to main
        performSegue(withIdentifier: "back-add-to-main", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var store_your_picture_b: UIButton!
    
    @IBAction func take_photo(_ sender: Any) {
        //If camera is OK
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image =  info[UIImagePickerControllerOriginalImage] as? UIImage{
            image_trip.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
