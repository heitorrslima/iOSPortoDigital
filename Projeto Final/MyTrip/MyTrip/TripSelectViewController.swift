//
//  TripSelectViewController.swift
//  MyTrip
//
//  Created by Treinamento on 16/06/2018.
//  Copyright © 2018 Treinamento. All rights reserved.
//

import UIKit

class TripSelectViewController: UIViewController {

    @IBOutlet weak var tripTitle: UINavigationItem!
    @IBOutlet weak var tripImage: UIImageView!
    @IBOutlet weak var tripDate: UIDatePicker!
    @IBOutlet weak var tripDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //restore index path
        let index = repositorio.getIndexTrip();
        
        //restpre trip element
        let trip = repositorio.getTripByIndex(index)
        
        //setThe
        tripTitle.title = trip.getName();
        tripDescription.text = trip.getDescription();
        tripImage.image = trip.getImage();
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
