//
//  TripRepository.swift
//  MyTrip
//
//  Created by Treinamento on 16/06/2018.
//  Copyright © 2018 Treinamento. All rights reserved.
//

import UIKit

class TripRepository: UIView {
    
    private var trips : [Trip] =
        [Trip("Rio de Janeiro", "Foi maravilhoso visitar ao Cristo Redentor e a Praia de Copa Cabana, apesar de sentir a Cidade Violenta", UIImage(named: "riodejaneiro")!), Trip("Curitiba", "Cidade limpa e organizada. Atualmente palco político desde o escandalo da Petrobras", UIImage(named: "curitiba")!)]
    private var indexTrip : Int = 0;
    //init the repository
    
    func getIndexTrip() -> Int{
        return self.indexTrip;
    }
    
    func setIndexTrip(_ indexTrip:Int){
        self.indexTrip = indexTrip
    }
    
    func getArray() -> [Trip]{
        return self.trips;
    }
    
    //add new elements in array
    func addTrip(trip: Trip) {
        trips.append(trip)
    }
    //get element by index
    func getTripByIndex(_ index: Int) -> Trip {
        return self.trips[index];
    }
    //override the trip edited
    func editTripByID(_ indexTripForEdit:Int, _ tripEdited:Trip){
        self.trips[indexTripForEdit] = tripEdited;
    }
    
    //remove the element of array and return this element
    func removeTripByID(_ indexTripForRemove:Int) -> Trip{
        return trips.remove(at: indexTripForRemove)
    }
    
    //edit element by Index
//    func editTripByIndex(_ at:Int) -> Bool {
//        for tripsIndex in 0...trips.count {
//            trips[tripsIndex];
//        }
//    }

}
