//
//  TripCoreDataHandler.swift
//  MyTrip
//
//  Created by Heitor Lima on 17/06/18.
//  Copyright © 2018 Treinamento. All rights reserved.
//

import UIKit
import CoreData

class TripCoreDataHandler: NSObject {
    
    private class func getContext() -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    class func editObject(_ trip: Trip, _ indexPathRow : Int){
        let context = getContext()
        var trips:[NSObject] = []
        //try return all objects stored
        }catch {
            print("erro for fetch data")
            return trips
        }
    }
    
    class func saveObject(_ name_trip:String, _ image_trip: UIImage, _ date_trip: Date, _ description_trip:String) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Trip", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(name_trip, forKey: "name_trip")
        manageObject.setValue(image_trip, forKey: "image_trip")
        manageObject.setValue(date_trip, forKey: "date_trip")
        manageObject.setValue(description_trip, forKey: "description_trip")
        
        do {
            try context.save()
            return true
        } catch {
            return false
        }
        
    }
    //return Trips CoreData
    class func fetchObject() -> [Trip]! {
        let context = getContext()
        var trips:[Trip] = []
        //try return all objects stored
        do {
            trips = try context.fetch(Trip.fetchRequest())
            return trips
        }catch {
            print("erro for fetch data")
            return trips
        }
    }
    //Delete a single object
    class func deleteObject(trip: Trip) -> Bool {
        let context = getContext()
        context.delete(trip)
        //try save
        do {
            try context.save()
            return true
        }catch{
            return false
        }
    }
}
