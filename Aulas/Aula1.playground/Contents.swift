//: Playground - noun: a place where people can play

import UIKit

//Exercicio comparando strings
var nome = "Heitor";
var sobrenome = "Heitor";

if(nome == sobrenome){
    print("nome e sobrenome nao podem ser iguais");
}else{
    print("nome e sobrenome estao ok");
}

//Exercicio de qtd de apartamentos por predio
let alturaPredio = 30;
let alturaApartamento = 3;
let areaPredioApartamento = 60;
let areaApartamento = 20;

var qtdApartamentoPorAndar = (areaPredioApartamento/areaApartamento);
var qtdApartamentos = (alturaPredio/alturaApartamento) * qtdApartamentoPorAndar;

print("Quantidade de Apartamentos no prédio é:",qtdApartamentos, "\nE a quantidade por andar é:", qtdApartamentoPorAndar);


//Exercicio de introducao ao Switch
var opcao = 1;

switch opcao {
case 1:
    print("opcao 1");
case 2:
    print("opcao 2");
default:
    print("opcao nao escolhida");
}


//While
var contador = 0;

while contador < 10{
    contador += 1;
}
print(contador)

//for-while
contador = 0;

repeat{
    contador += 1
}while contador < 10
print(contador)

//funcao
func maior (_ primeiro: Int, _ segundo:Int) -> Int {
    if(primeiro > segundo){
        return primeiro;
    }
    return segundo;
}
print("O maior numero é:", maior(4 ,5))

//struct (Estrutura)
struct Pessoa{
    var nome:String;
    var idade:Int;
    var cpf:String;
    
}

var heitor = Pessoa(nome:"Heitor Rodrigues de Sousa Lima", idade:22, cpf:"088.433.424-44")

print(heitor)


func isAnoBissexto(ano:Int) -> Bool{
    if(ano%4 == 0){
        if(ano%100 == 0){
            if(ano%400 == 0){
                return true
            }
        }
        return true
    }
    return false
}

isAnoBissexto(ano: 2020)

func anosBissextosNoIntervalo(ano1:Int, ano2:Int)->Int{
    var qtdAnosBi = 0;
    for anoAtual in ano1...ano2{
        if(isAnoBissexto(ano: anoAtual)){
            print(anoAtual, "é bissexto");
            qtdAnosBi += 1;
        }
    }
    return qtdAnosBi;
}

anosBissextosNoIntervalo(ano1: 2000, ano2: 2018)


