//
//  ViewController.swift
//  QuintaAula
//
//  Created by Treinamento on 28/04/2018.
//  Copyright © 2018 Treinamento Porto Digital. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var nome: String!
    var email: String!
    var imagem: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nomeLabel.text = nome
        emailLabel.text = email
        
        //self.scrollView.isDirectionalLockEnabled = true
        //self.scrollView.alwaysBounceHorizontal = false
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

