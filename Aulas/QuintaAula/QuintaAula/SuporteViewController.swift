//
//  SuporteViewController.swift
//  QuintaAula
//
//  Created by Treinamento on 28/04/2018.
//  Copyright © 2018 Treinamento Porto Digital. All rights reserved.
//

import UIKit

class SuporteViewController: UIViewController {
    
    @IBOutlet weak var nome: UITextField!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ segue: UIStoryboardSegue){
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let proximaView = segue.destination as? ViewController else {
            print("ERRO")
            return
        }
        proximaView.nome = self.nome.text
        proximaView.email = self.email.text
        //proximaView.imagem = self.image2.animationImages
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
