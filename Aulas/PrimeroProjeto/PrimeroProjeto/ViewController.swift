//
//  ViewController.swift
//  PrimeroProjeto
//
//  Created by Treinamento on 24/03/2018.
//  Copyright © 2018 Treinamento Porto Digital. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var l_digitar_senha: UILabel!
    @IBOutlet weak var tf_senha: UITextField!
    
    @IBOutlet weak var l_senha1: UILabel!
    @IBOutlet weak var l_senha2: UILabel!
    @IBOutlet weak var l_senha3: UILabel!
    @IBOutlet weak var l_senha4: UILabel!
    @IBOutlet weak var l_senha5: UILabel!
    
    @IBOutlet weak var l_confirmacao: UILabel!
    
    @IBAction func b_enviar(_ sender: Any) {
        
        if l_senha1.text == "senha1"{
            l_senha1.text = tf_senha.text
        }else if l_senha2.text == "senha2"{
            l_senha2.text = tf_senha.text
        }else if l_senha3.text == "senha3"{
            l_senha3.text = tf_senha.text
        }else if l_senha4.text == "senha4"{
            l_senha4.text = tf_senha.text
        }else if l_senha5.text == "senha5"{
            l_senha5.text = tf_senha.text
        }else if senhas[0]==l_senha1.text &&
                    senhas[1]==l_senha2.text &&
                    senhas[2]==l_senha3.text &&
                    senhas[3]==l_senha4.text &&
                    senhas[4]==l_senha5.text{
            l_confirmacao.text = "OK"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

