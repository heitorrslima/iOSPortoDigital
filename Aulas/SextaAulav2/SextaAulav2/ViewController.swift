//
//  ViewController.swift
//  SextaAulav2
//
//  Created by Treinamento on 05/05/2018.
//  Copyright © 2018 Treinamento Porto Digital. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!

    var data: [String] = ["Heitor Barcelona", "Rogerio Lito", "Vitória Moura", "Antonio Marcos", "Maria Luiza", "Sávio Meirelles", "Gisele Meneses", "Maria Loila", "Maria Helena"]
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celula-x", for: indexPath)
        cell.textLabel?.text = data[indexPath.row]
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //let nextView = segue.destination as! ViewController2
        if(segue.identifier == "seta1"){
            let senderCell = sender as! UITableViewCell
            let secondView = segue.destination as! ViewController2
            secondView.nomeCompleto = senderCell.textLabel?.text
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

