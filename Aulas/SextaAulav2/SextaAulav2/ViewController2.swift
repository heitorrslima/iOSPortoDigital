//
//  ViewController2.swift
//  SextaAulav2
//
//  Created by Treinamento on 05/05/2018.
//  Copyright © 2018 Treinamento Porto Digital. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    var nomeCompleto: String!
    
    //Variáveis TEXT
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var nome: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nome.text = nomeCompleto

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
