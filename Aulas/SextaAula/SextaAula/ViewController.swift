//
//  ViewController.swift
//  SextaAula
//
//  Created by Treinamento on 05/05/2018.
//  Copyright © 2018 Treinamento Porto Digital. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://ultramatercursos.com.br")
        if let unwrapedUrl = url {
            let request = URLRequest(url: unwrapedUrl)
            let session = URLSession.shared
            let task = session.dataTask(with: request)  { (_, _, error) in
                if error == nil {
                    DispatchQueue.main.async {
                        self.webView.loadRequest(request)
                    }
                }
            }
            task.resume()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

