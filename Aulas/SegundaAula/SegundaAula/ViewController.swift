//
//  ViewController.swift
//  SegundaAula
//
//  Created by Treinamento on 07/04/2018.
//  Copyright © 2018 Treinamento Porto Digital. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var arrayPessoa:[NSManagedObject] = []

    override func viewDidLoad() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let pessoa_entidade = NSEntityDescription.entity(forEntityName: "Pessoa", in: managedContext)
        
        arrayPessoa =
        
        for pessoa in arrayPessoa{
            print(pessoa.value(forKey: "nome")!)
        }
        
        let pessoa1 = NSManagedObject(entity: pessoa_entidade!, insertInto: managedContext)
        pessoa1.setValue("HEITOR", forKey: "nome")
        pessoa1.setValue(23, forKey: "idade")
        
        salvar(managedContext)//Salva o model
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func fetch(_ context: NSManagedObjectContext, _ entityName:String) -> [NSManagedObject] {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        do {
            let object = try context.fetch(fetchRequest)
            return object
        } catch let error as NSError {
            print("Nao foi possivel salvar. \(error)")
            return []
        }
    }
    
    func salvar(_ context:NSManagedObjectContext){
        do {
            try context.save()
            print("Salvo com sucesso!")
        } catch let error as NSError {
            print("Náo foi possivel salvar :( \(error)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

