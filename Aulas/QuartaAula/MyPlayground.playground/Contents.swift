//: Playground - noun: a place where people can play

import UIKit

protocol PessoaProtocol{
    var nome: String {get}
    var idade: Int {get}
    var cpf: String {get}
}

protocol FuncionarioProtocol{
    var salario: Double {get}
    var vale_refeicao: Double {get}
}

protocol GerenteProtocol{
    var taxa_bonificacao: Double {get}
}

//Dados basicos de toda pessoa
class Pessoa: PessoaProtocol{
    
    var nome: String
    var idade: Int
    var cpf: String
    
    init(nome: String, idade: Int, cpf: String){
        self.nome = nome
        self.idade = idade
        self.cpf = cpf
    }
    
}

//Classe Funcionario herda de Pessoas, pois em sua essencia é uma pessoa
class Funcionario: Pessoa, FuncionarioProtocol{
    
    //Variáveis de Funcionario
    var salario: Double
    var vale_refeicao: Double
    
    //Inicializador de Funcionário
    init(nome: String, idade: Int, cpf: String, salario: Double, vale_refeicao:Double){
        self.salario = salario
        self.vale_refeicao = vale_refeicao
        super.init(nome: nome, idade: idade, cpf: cpf)
    }
    
    init(pessoa: Pessoa, salario: Double, vale_refeicao:Double){
        self.salario = salario
        self.vale_refeicao = vale_refeicao
        super.init(nome: pessoa.nome, idade: pessoa.idade, cpf: pessoa.cpf)
    }
    
    //Retorna salário
    func getSalario() -> Double{
        return salario
    }
}

//Todo gerente é um funcionario, logo herda de Funcionario
class Gerente: Funcionario{
    
    //Porcentagem de Bonificacao (0.00 - 1.00)
    var taxa_bonificacao: Double
    
    init(nome: String, idade: Int, cpf:String, salario: Double, vale_refeicao:Double, taxa_bonificacao: Double){
        self.taxa_bonificacao = taxa_bonificacao
        super.init(nome: nome, idade: idade, cpf: cpf, salario: salario, vale_refeicao: vale_refeicao)
    }
    
    init(funcionario: Funcionario, taxa_bonificacao: Double){
        self.taxa_bonificacao = taxa_bonificacao
        super.init(nome: funcionario.nome, idade: funcionario.idade, cpf: funcionario.cpf, salario: funcionario.salario, vale_refeicao: funcionario.vale_refeicao)
    }
    
    //retorna salario ajustado com a taxa de bonificacao
    override func getSalario() -> Double {
        return (super.getSalario()*taxa_bonificacao)+super.getSalario()
    }
}

//Inicializacao de pessoas
var pessoa1 = Pessoa(nome: "Heitor", idade: 29, cpf: "088.433.424-44")
var pessoa2 = Pessoa(nome: "Antonio", idade: 35, cpf: "123.333.455.77")

var funcionario1 = Funcionario(pessoa: pessoa1, salario: 6700, vale_refeicao: 780)
print(funcionario1.getSalario())

var funcionario2 = Funcionario(pessoa: pessoa1, salario: 8799, vale_refeicao: 780)
print(funcionario2.getSalario())

var gerente = Gerente(funcionario: funcionario2, taxa_bonificacao:0.1)
print(gerente.getSalario())

